#### BiDirectional Syncing
![bidirectional-sync](https://gitlab.com/jomarquez/bidirectional-syncing/uploads/a5cb74bc83c5bfac1a5f2e89104c0ee8/git-flow__1_.png)

1. On code push to unprotected branches (feature/#{feature_name} above), we will automatically fetch **open-source** `production` into **internal** `public-production`. A dev will create an MR to merge `public-production` into `development` periodically. 

2. On prod deploy, dev will create an MR to merge **internal** `production` into `public-production`. This is a manual process so dev can review code before push to **open-source**. On merge, the manual `public-production-push` job will become available for execute.


### Requirements
1. Internal and External project url
2. SSH Key Pairs (Write to Each Repo)
3. Access Token (Merge Request)
4. Save Gitlab CI Variables
5. Internal `public-production` protected branch
6. Add Gitlab Jobs


#### SSH Key Pairs

Create SSH Key pairs and share public SSH key to each repo.
You need this to be able to write to each repo.

```console
ssh-keygen -o -t rsa -b 4096 -C "jjohnson@iqt.org"
Enter file in which to save the key: 
```

| Repo | Key Name | 
| ------ | ------ |
| `Internal Project` | ~/.ssh/internal.key |
| `External Project` | ~/.ssh/external.key |


##### Add Deploy Key to each repo

* [ ] Go to Repo settings -> CI/CD -> Deploy Keys
* [ ] Copy your public key: `pbcopy < ~/.ssh/${key_name}.key.pub`
* [ ] Paste your machine public key in the Key field and check Write access allowed


| Repo | Name | Key | Write access allowed |
| ------ | ------ | ------ | ------ |
| `Internal Project` | Internal Key | `pbcopy < ~/.ssh/internal.key.pub` | :white_check_mark: |
| `External Project` | External Key | `pbcopy < ~/.ssh/external.key.pub` | :white_check_mark: |


#### Add Personal Access Token
Create an access token and save to GitLab CI Variables. The automerge-request.sh file reads in this var. 
You need it to be able to create a merge request via GitLab CI Rest API.

`auto-merge-request` ci/cd job automatically creates a merge request when internal `public-production` is updated. to access the GitLab API we need to generate a generate a personal access token which scopes the api 
![add personal access token](https://gitlab.iqt.org/dev/arq/arq-client/uploads/afdc0713622f10feff6858acb2d64984/Screen_Shot_2020-03-17_at_6.21.07_PM.png)


#### Add CI/CD ENV Variables to `Internal` Repo

* [ ] Go to Repo settings -> CI/CD -> Variables

| CI/CD ENV | Key Name | 
| ------ | ------ |
| INTERNAL_SSH_PRIVATE_KEY | `pbcopy < ~/.ssh/internal.key` |
| EXTERNAL_SSH_PRIVATE_KEY | `pbcopy < ~/.ssh/external.key` |
| PERSONAL_ACCESS_TOKEN | copy from Add Personal Access Token step above |
| INTERNAL_SSH_URL | git@gitlab.internal.org:dev/project_space/project.git |
| EXTERNAL_SSH_URL |  git@gitlab.com:project_space/project.git | 




#### Add internal:`public-production` to Internal Protected Branches
Your `public-production` branch needs to be protected to have access to Gitlab CI Variables.

Under [settings->Repository->Protected Branches](https://gitlab.iqt.org/dev/arq/arq-client/-/settings/repository)

#### Update .gitlab-cy.yml
Update .gitlab-cy.yml jobs to meet your needs. The `public-production-manual-push` job
checks out your `public-production` and pushes it to the external remote.

`open-source-production-auto-fetch` job fetches and merges your external `production` branch 
to your internal `public-production` branch


#### Update Custom CI configuration path on External Repo
Your external gitlab-ci yaml file should be a different filename than your internal one.

OpenSource Repo -> Settings -> CI/CD -> General Pipelines -> 

`Custom CI configuration path`: .gitlab-ci.opensource.yml


#### References
https://www.iqt.org/arq-gatekeeping/
